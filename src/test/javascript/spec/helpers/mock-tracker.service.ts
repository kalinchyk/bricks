import { SpyObject } from './spyobject';
import { BricksTrackerService } from '../../../../main/webapp/app/shared/tracker/tracker.service';

export class MockTrackerService extends SpyObject {

    constructor() {
        super(BricksTrackerService);
    }

    connect () {}
}
