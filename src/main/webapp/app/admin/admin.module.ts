import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ParseLinks } from 'ng-jhipster';
import { BricksTrackerService } from './../shared/tracker/tracker.service';

import { BricksSharedModule } from '../shared';

import {
    adminState,
    AuditsComponent,
    UserMgmtComponent,
    UserDialogComponent,
    UserDeleteDialogComponent,
    UserMgmtDetailComponent,
    UserMgmtDialogComponent,
    UserMgmtDeleteDialogComponent,
    LogsComponent,
    BricksMetricsMonitoringModalComponent,
    BricksMetricsMonitoringComponent,
    BricksHealthModalComponent,
    BricksHealthCheckComponent,
    BricksConfigurationComponent,
    BricksDocsComponent,
    AuditsService,
    BricksConfigurationService,
    BricksHealthService,
    BricksMetricsService,
    BricksTrackerComponent,
    LogsService,
    UserResolvePagingParams,
    UserResolve,
    UserModalService
} from './';


@NgModule({
    imports: [
        BricksSharedModule,
        RouterModule.forRoot(adminState, { useHash: true })
    ],
    declarations: [
        AuditsComponent,
        UserMgmtComponent,
        UserDialogComponent,
        UserDeleteDialogComponent,
        UserMgmtDetailComponent,
        UserMgmtDialogComponent,
        UserMgmtDeleteDialogComponent,
        LogsComponent,
        BricksConfigurationComponent,
        BricksHealthCheckComponent,
        BricksHealthModalComponent,
        BricksDocsComponent,
        BricksTrackerComponent,
        BricksMetricsMonitoringComponent,
        BricksMetricsMonitoringModalComponent
    ],
    entryComponents: [
        UserMgmtDialogComponent,
        UserMgmtDeleteDialogComponent,
        BricksHealthModalComponent,
        BricksMetricsMonitoringModalComponent,
    ],
    providers: [
        AuditsService,
        BricksConfigurationService,
        BricksHealthService,
        BricksMetricsService,
        LogsService,
        BricksTrackerService,
        UserResolvePagingParams,
        UserResolve,
        UserModalService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BricksAdminModule {}
