import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { BricksHealthCheckComponent } from './health.component';

export const healthRoute: Route = {
  path: 'bricks-health',
  component: BricksHealthCheckComponent,
  data: {
    pageTitle: 'health.title'
  }
};
