import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { BricksConfigurationComponent } from './configuration.component';

export const configurationRoute: Route = {
  path: 'bricks-configuration',
  component: BricksConfigurationComponent,
  data: {
    pageTitle: 'configuration.title'
  }
};
