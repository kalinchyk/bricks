import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { BricksMetricsMonitoringComponent } from './metrics.component';

export const metricsRoute: Route = {
  path: 'bricks-metrics',
  component: BricksMetricsMonitoringComponent,
  data: {
    pageTitle: 'metrics.title'
  }
};
