import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { BricksTrackerComponent } from './tracker.component';
import { BricksTrackerService, Principal } from '../../shared';

export const trackerRoute: Route = {
  path: 'bricks-tracker',
  component: BricksTrackerComponent,
  data: {
    pageTitle: 'tracker.title'
  }
};
