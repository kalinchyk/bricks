import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { BricksDocsComponent } from './docs.component';

export const docsRoute: Route = {
  path: 'docs',
  component: BricksDocsComponent,
  data: {
    pageTitle: 'global.menu.admin.apidocs'
  }
};
