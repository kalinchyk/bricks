import { Component } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';

@Component({
    selector: 'bricks-docs',
    templateUrl: './docs.component.html'
})
export class BricksDocsComponent {
    constructor (
        private jhiLanguageService: JhiLanguageService
    ) {
        this.jhiLanguageService.setLocations(['global']);
    }
}
