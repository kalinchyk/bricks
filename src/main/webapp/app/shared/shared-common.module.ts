import { NgModule, Sanitizer } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { TranslateService } from 'ng2-translate';
import { AlertService } from 'ng-jhipster';

import {
    BricksSharedLibsModule,
    JhiLanguageHelper,
    FindLanguageFromKeyPipe,
    BricksAlertComponent,
    BricksAlertErrorComponent
} from './';


export function alertServiceProvider(sanitizer: Sanitizer, translateService: TranslateService) {
    // set below to true to make alerts look like toast
    let isToast = false;
    return new AlertService(sanitizer, isToast, translateService);
}

@NgModule({
    imports: [
        BricksSharedLibsModule
    ],
    declarations: [
        FindLanguageFromKeyPipe,
        BricksAlertComponent,
        BricksAlertErrorComponent
    ],
    providers: [
        JhiLanguageHelper,
        {
            provide: AlertService,
            useFactory: alertServiceProvider,
            deps: [Sanitizer, TranslateService]
        },
        Title
    ],
    exports: [
        BricksSharedLibsModule,
        FindLanguageFromKeyPipe,
        BricksAlertComponent,
        BricksAlertErrorComponent
    ]
})
export class BricksSharedCommonModule {}
