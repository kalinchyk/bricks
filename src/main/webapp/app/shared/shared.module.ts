import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import { CookieService } from 'angular2-cookie/services/cookies.service';
import {
    BricksSharedLibsModule,
    BricksSharedCommonModule,
    CSRFService,
    AuthService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    Principal,
    BricksTrackerService,
    HasAnyAuthorityDirective,
    BricksSocialComponent,
    SocialService,
    BricksLoginModalComponent
} from './';

@NgModule({
    imports: [
        BricksSharedLibsModule,
        BricksSharedCommonModule
    ],
    declarations: [
        BricksSocialComponent,
        BricksLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        CookieService,
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        BricksTrackerService,
        AuthServerProvider,
        SocialService,
        AuthService,
        UserService,
        DatePipe
    ],
    entryComponents: [BricksLoginModalComponent],
    exports: [
        BricksSharedCommonModule,
        BricksSocialComponent,
        BricksLoginModalComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class BricksSharedModule {}
