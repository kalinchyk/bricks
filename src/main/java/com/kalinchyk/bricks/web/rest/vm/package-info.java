/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kalinchyk.bricks.web.rest.vm;
