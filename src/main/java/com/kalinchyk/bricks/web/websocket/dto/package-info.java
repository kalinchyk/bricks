/**
 * Data Access Objects used by WebSocket services.
 */
package com.kalinchyk.bricks.web.websocket.dto;
