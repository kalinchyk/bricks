package com.kalinchyk.bricks.integration.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.List;

/**
 * Created by vya on 04.03.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PartCategories extends AbstractResult {
    private List<PartCategory> results;

    public List<PartCategory> getResults() {
        return results;
    }

    public void setResults(List<PartCategory> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "PartCategories{" +
                "results=" + results +
                "} " + super.toString();
    }
}
