package com.kalinchyk.bricks.integration.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.time.ZonedDateTime;

/**
 * Created by vya on 04.03.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Set {
    private String setNum;
    private String name;
    private Integer year;
    private Integer themeId;
    private Integer numParts;
    private String setImgUrl;
    private String setUrl;
    private ZonedDateTime lastModifiedDt;

    public String getSetNum() {
        return setNum;
    }

    public void setSetNum(String setNum) {
        this.setNum = setNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getThemeId() {
        return themeId;
    }

    public void setThemeId(Integer themeId) {
        this.themeId = themeId;
    }

    public Integer getNumParts() {
        return numParts;
    }

    public void setNumParts(Integer numParts) {
        this.numParts = numParts;
    }

    public String getSetImgUrl() {
        return setImgUrl;
    }

    public void setSetImgUrl(String setImgUrl) {
        this.setImgUrl = setImgUrl;
    }

    public String getSetUrl() {
        return setUrl;
    }

    public void setSetUrl(String setUrl) {
        this.setUrl = setUrl;
    }

    public ZonedDateTime getLastModifiedDt() {
        return lastModifiedDt;
    }

    public void setLastModifiedDt(ZonedDateTime lastModifiedDt) {
        this.lastModifiedDt = lastModifiedDt;
    }

    @Override
    public String toString() {
        return "Set{" +
                "setNum='" + setNum + '\'' +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", themeId=" + themeId +
                ", numParts=" + numParts +
                ", setImgUrl='" + setImgUrl + '\'' +
                ", setUrl='" + setUrl + '\'' +
                ", lastModifiedDt=" + lastModifiedDt +
                '}';
    }
}
