package com.kalinchyk.bricks.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vya on 04.03.2017.
 */
@RestController
@RequestMapping("/api/rebrickable")
public class RebrickableController {
    private final Logger log = LoggerFactory.getLogger(RebrickableController.class);

    private final RebrickableService rebrickableService;

    public RebrickableController(RebrickableService rebrickableService) {
        this.rebrickableService = rebrickableService;
    }

    @GetMapping("/themes")
    public void syncThemes() {
        rebrickableService.themes();
    }

    @GetMapping("/sets")
    public void syncSets() {
        rebrickableService.sets();
    }

    @GetMapping("/colors")
    public void syncColors() {
        rebrickableService.colors();
    }

    @GetMapping("/parts")
    public void syncParts() {
        rebrickableService.parts();
    }

    @GetMapping("/part_categories")
    public void syncPartCategories() {
        rebrickableService.partCategories();
    }

}
