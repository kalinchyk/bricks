package com.kalinchyk.bricks.integration;

import com.kalinchyk.bricks.integration.dto.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by vya on 04.03.2017.
 */
@Service
public class RebrickableService {
    private String apiUrl = "http://rebrickable.com/api/v3/lego/%s/?key=77c7242e21df26c4fa4fba1636d2d286";
    private RestTemplate restTemplate = new RestTemplate();

    public void themes() {
        Themes themes = restTemplate.getForObject(String.format(this.apiUrl, "themes"), Themes.class);
        System.out.println(themes.toString());
    }

    public void sets() {
        Sets sets = restTemplate.getForObject(String.format(this.apiUrl, "sets"), Sets.class);
        System.out.println(sets.toString());
    }

    public void colors() {
        Colors colors = restTemplate.getForObject(String.format(this.apiUrl, "colors"), Colors.class);
        System.out.println(colors.toString());
    }

    public void parts() {
        Parts parts = restTemplate.getForObject(String.format(this.apiUrl, "parts"), Parts.class);
        System.out.println(parts.toString());
    }

    public void partCategories() {
        PartCategories partCategories = restTemplate.getForObject(String.format(this.apiUrl, "part_categories"), PartCategories.class);
        System.out.println(partCategories.toString());
    }

//    @Scheduled(cron = "00 */2 * * * ?")
    public void syncThemes() {
        this.themes();
    }

//    @Scheduled(cron = "05 */2 * * * ?")
    public void syncSets() {
        this.sets();
    }

//    @Scheduled(cron = "10 */2 * * * ?")
    public void syncColors() {
        this.colors();
    }

//    @Scheduled(cron = "15 */2 * * * ?")
    public void syncParts() {
        this.parts();
    }

//    @Scheduled(cron = "20 */2 * * * ?")
    public void syncPartCategories() {
        this.partCategories();
    }
}
